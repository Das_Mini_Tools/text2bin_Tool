﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace text2bin_Tool
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Error();
                Environment.Exit(1);
            }
            else if (args.Length == 2)
            {
                if (!File.Exists(args[0]))
                {
                    Error();
                    Environment.Exit(2);
                }

                using (StreamReader reader = new StreamReader(args[0]))
                using (FileStream writer = new FileStream(args[1], FileMode.Create, FileAccess.Write, FileShare.Write, 4096, FileOptions.SequentialScan))
                {
                    char[] buffer = new char[4096];

                    int readChars = 0;

                    while ((readChars = reader.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        string bufferStr = new string(buffer, 0, readChars);

                        writer.Write(HexToByteArray(bufferStr), 0, bufferStr.Length / 2);
                    }
                }

                return;
            }
        }

        private static void Error()
        {
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);

            Console.WriteLine(Environment.NewLine + "{0} {1} by {2}, written for catalinnc." + Environment.NewLine, fvi.ProductName, fvi.ProductVersion, fvi.CompanyName);

            Console.WriteLine("  -v         --version{0,35}", "Shows the version information.");
            Console.WriteLine("  inputfile  outputfile{0,56}", "Writes the byte values contained in inputfile's");
            Console.WriteLine("{0,47}", "bytes to outputfile.");
        }

        private static byte[] HexToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}
